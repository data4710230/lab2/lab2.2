/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.lab2;

/**
 *
 * @author Tauru
 */
public class RemoveElement {

    public static int removeElement(int[] nums, int val) {
        int k = 0;  // Number of elements not equal to val
        for (int i = 0; i < nums.length; i++) {
            if (nums[i] != val) {
                nums[k] = nums[i];
                k++;
            }
        }
        return k;
    }

    public static void main(String[] args) {
        int[] nums = {0,1,2,2,3,0,4,2};
        int val = 2;

        int result = removeElement(nums, val);

        System.out.println("Output: " + result);
        System.out.print("nums = [");
        for (int i = 0; i < result; i++) {
            System.out.print(nums[i]);
            if (i < result - 1) {
                System.out.print(", ");
            }
        }
        for (int i = result; i < nums.length; i++) {
            System.out.print("_, ");
        }
        System.out.println("]");
    }
}

